#include "Balance.h"
#include <vector>
#include <algorithm>
#include <map>
bool Balancer::isBalances(string s)
{
    map<int, char> open_brack = {{'{', 1}, {'(', 2}, {'[', 3}};
    map<int, char> close_brack = {{'}', 1}, {')', 2}, {']', 3}};
    for (char i: s)
    {
        if (open_brack.find(i) != open_brack.end()) body.push_back(open_brack[i]);
        if (close_brack.find(i) != close_brack.end())
        {
            if (body.size() == 0) return false;
            if (body[body.size()-1] == close_brack[i]) body.pop_back();
            else return false;

        };
    }
    if (body.size() != 0) return false;
    return true;
}