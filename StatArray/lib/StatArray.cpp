#include <fstream>
#include <algorithm>
#include <cmath>
#include "StatArray.h"

int StatArray::min()
{
    return body[0];
}
int StatArray::max()
{
    return body[body.size()-1];
}
double StatArray::mean()
{
    double res;
    for (int i: body) res+=i;
    return res/body.size();
}
double StatArray::rms()
{
    double res;
    double tmp = mean();
    for (int i: body) res+=(i-tmp)*(i-tmp);
    return sqrt(res);
}
size_t StatArray::countLarger(int a)
{
    if (body.size() <= 2)
    {
        if (body[0] >= a) return 2;
        if (body[1] >= a) return 1;
        if (body[1] < a) return 0;
    }
    else
    {
        return distance(body.begin(), upper_bound(body.begin(), body.end(), a));
    }    
};

void StatArray::RFF(string name)
{
    ifstream fin;
    try
    {
        fin.open(name);
        if (fin.is_open() == false) throw runtime_error ("Input error: file not found or type is incorrect!");
        int c, n;
        fin >> n;
        for (int i = 0; i < n; i++)
        {
            fin >> c;
            body.push_back(c);
        }
    }
    catch (int e)
    {
        cout << e;
    }
    sort(body.begin(), body.end());
}
void StatArray::print()
{
    for(const auto &i: body) cout << i << " ";
}