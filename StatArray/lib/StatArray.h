#include <vector>
#include <iostream>
using namespace std;

class StatArray
{
    public:
        int max();
        int min();
        double mean();
        double rms();
        size_t countLarger(int a);
        void RFF(string name);
        void print();
        
    private:
        vector<int> body;
};