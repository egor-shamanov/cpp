import copy
with open("output.txt", "w") as f:
    arr = []
    n = int(2**12.5)
    for i in range(2**25):
        arr.append(True)
    for i in range(2, n):
        if arr[i]:
            for j in range (i**2, n**2, i):
                arr[j] = False
    
    for i in range(len(arr)):
        if arr[i] and i >= 2:
            f.write(str(i) + " ")