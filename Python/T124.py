from tkinter import *
import copy
import time

field = []
for i in range(32):
    field.append([])
    for j in range(24):
        field[i].append(0)


zero = copy.deepcopy(field)
step = copy.deepcopy(field)

squares = []
 
def exitWin(event):
    root.destroy()

val = 25

 
def b1(event):
    print(event.x//25, " ", event.y//25)
    if event.x > 600:
        pass
    else:
        if field[event.y//25][event.x//25] == 0:
            field[event.y//25][event.x//25] = 1
        else:
            field[event.y//25][event.x//25] = 0
        if field[event.y//25][event.x//25] == 1:
            c.itemconfig(squares[event.y//25][event.x//25], fill = "green")
        else:
            c.itemconfig(squares[event.y//25][event.x//25], fill = "white")

begin = False

def start():
    global begin
    begin = True

def stop():
    global begin
    begin = False

def erase():
    global field
    field = copy.deepcopy(zero)
    draw()

def iterate():
    global field, begin
    step = copy.deepcopy(field)
    if begin:
        for i in range(len(field)):
            for j in range(len(field[i])):
                neigbours = 0
                if (not ((i+1) >= 32)) and (not ((j+1) >= 24)):
                    neigbours += field[i+1][j+1]

                if not (i+1) >= 32 and not (j) >= 24:
                    neigbours += field[i+1][j]

                if not (i+1) >= 32 and not (j-1) < 0:
                    neigbours += field[i+1][j-1]

                if not (i) >= 32 and not (j-1) < 0:
                    neigbours += field[i][j-1]

                if not (i) >= 32 and not (j+1) >= 24:
                    neigbours += field[i][j+1]

                if not (i-1) < 0 and not (j-1) < 0:
                    neigbours += field[i-1][j-1]

                if not (i-1) < 0 and not (j+1) >= 24:
                    neigbours += field[i-1][j+1]

                if not (i-1) < 0 and not (j) >= 24:
                    neigbours += field[i-1][j]
                
                if neigbours > 3 or neigbours < 2:
                    step[i][j] = 0
                elif neigbours == 3:
                    step[i][j] = 1
                else:
                    print(neigbours)
                    step[i][j] = step[i][j]
        field = copy.deepcopy(step)
        draw()
    c.after(1000, iterate)



def draw():
    for i in range(len(field)):
        for j in range(len(field[i])):
            if field[i][j] == 1:
                c.itemconfig(squares[i][j], fill = "green")
            else:
                c.itemconfig(squares[i][j], fill = "white")

root = Tk()

global c
c = Canvas(root, width=1000, height=600, bg='white')
c.pack()
for i in range(len(field)):
    squares.append([])
    for j in range(len(field[i])):
        if field[i][j] == 0:
            squares[i].append(c.create_rectangle(j*25, i*25, (j+1)*25, (i+1)*25, fill='white', outline='black',width=1))
        if field[i][j] == 1:
            squares[i].append(c.create_rectangle(j*25, i*25, (j+1)*25, (i+1)*25, fill='green', outline='black',width=1))

root.resizable(False, False)

but = Button(root, text = "Start simulation", command = start)
but.place(x = 750, y = 50, width = 150, height = 50)

but1 = Button(root, text = "Stop simulation", command = stop)
but1.place(x = 750, y = 150, width = 150, height = 50)

but2 = Button(root, text = "Erase field", command = erase)
but2.place(x = 750, y = 250, width = 150, height = 50)

root.bind('<Button-3>', b1)
c.after(1000, iterate())

root.mainloop()
