#include "../lib/exoplanet.h"
#include "../lib/gas_giant.h"
#include "../lib/star.h"

#include <string>
#include <vector>
#include <iostream>
#include <stdint.h>
#include <algorithm>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <atomic>
#include <pthread.h>
#include <future>
#include <exception>
#include <stdexcept>
#include <fstream>

bool compare(planet* a, planet* b)
        {
            if (a->mass > b->mass) return true;
            else return false;
        };


int main()
{
    vector<planet*> solar_system;
    for (int i = 0; i < 10; i++)
    {
        int type = i%2;
        if (type == 0)
        {   exoplanet* tmp = new exoplanet();
            tmp->mass = rand()%5+1;
            tmp->code = "M"+to_string(rand()%9000+1000);
            tmp->atmosphere.push_back(rand()%100);
            tmp->atmosphere.push_back(100 - tmp->atmosphere[0]);
            solar_system.push_back(tmp);
            //tmp.add_sattelit();
        }
        if (type == 1)
        {
            gas_giant* tmp = new gas_giant();
            tmp->mass = rand()%50+6;
            tmp->code = "G"+to_string(rand()%9000+1000);
            solar_system.push_back(tmp);
            //tmp.add_sattelit();
        }
    }

    
    sort(solar_system.begin(), solar_system.end(), compare);

    for (int i = 0; i < 10; i++)
    {
        solar_system[i] ->add_sattelit();
        //if (exoplanet* ex = dynamic_cast<exoplanet*>(solar_system[0])) ex->add_sattelit();
        //if (gas_giant* ex = dynamic_cast<gas_giant*>(solar_system[0])) ex->add_sattelit();
    }
}

