#pragma once
#include <iostream>
#include <string>
#include "sattelite.h"
using namespace std;

class planet
{
    public:
        virtual void add_sattelit() = 0;
        float mass;
        string name, code;
        
    protected:
        int radius, apocenter, pericenter;
};