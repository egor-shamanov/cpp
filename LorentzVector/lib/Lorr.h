#include <iostream>
using namespace std;

class Lorentz
{
    public:
    Lorentz(){x = 0; y = 0; z = 0; w = 0;};
    Lorentz(float a, float b, float c, float d);


        Lorentz& operator = (Lorentz eq1)
        {
            if (this == &eq1) return *this;
            else
            {
                 x = eq1.x;
                 y = eq1.y;
                 z = eq1.z;
                 w = eq1.w;
                 return *this;
            }
        }
    //void convert(Lorentz x);

    friend ostream& operator << (ostream &c, Lorentz l);
    friend istream& operator >> (istream &c, Lorentz &l);
    Lorentz operator + (const Lorentz &eq1)
    {
        Lorentz res;
        res.x = eq1.x+this->x;
        res.y = eq1.y+this->y;
        res.z = eq1.z+this->z;
        res.w = eq1.w + this->w;
        return res;
    };
    Lorentz& operator - (const Lorentz &eq1)
    {
        x = eq1.x-this->x;
        y = eq1.y-this->y;
        z = eq1.z-this->z;
        w = eq1.w-this->w;
        return *this;
    };
    friend float operator * (Lorentz eq1, Lorentz eq2);
    friend bool operator == (Lorentz eq1, Lorentz eq2);
    friend bool operator >= (Lorentz eq1, Lorentz eq2);
    friend bool operator <= (Lorentz eq1, Lorentz eq2);
    private:
        float x, y, z, w;
};