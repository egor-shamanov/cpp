#include <cmath>
#include <iostream>
using namespace std;
#include "../lib/Lorr.h"


ostream& operator << (ostream &c, Lorentz l)
{
    c << "("<< l.x << ", " << l.y << ", " << l.z << ", " << l.w << ")";
    return c;
}
istream& operator >> (istream &c, Lorentz &l)
{

    c >> l.x;
    c >> l.y;
    c >> l.z;
    c >> l.w;
    return c;
}

float operator * (Lorentz eq1, Lorentz eq2)
{
    float eq_res = eq1.x*eq2.x + eq1.y*eq2.y + eq1.z*eq2.z + eq1.w*eq2.w;
    return eq_res;
}

bool operator == (Lorentz eq1, Lorentz eq2)
{
    return ((eq1.x==eq2.x)&&(eq1.y+eq2.y)&&(eq1.z+eq2.z)&&(eq1.w + eq2.w));
}
bool operator >= (Lorentz eq1, Lorentz eq2)
{
    float alpha = pow(eq1.x, 2)+pow(eq1.y, 2)+pow(eq1.z, 2)+pow(eq1.w, 2); 
    float beta = pow(eq2.x, 2)+pow(eq2.y, 2)+pow(eq2.z, 2)+pow(eq2.w, 2); 
    return (alpha >= beta);
}
bool operator <= (Lorentz eq1, Lorentz eq2)
{
    float alpha = pow(eq1.x, 2)+pow(eq1.y, 2)+pow(eq1.z, 2)+pow(eq1.w, 2); 
    float beta = pow(eq2.x, 2)+pow(eq2.y, 2)+pow(eq2.z, 2)+pow(eq2.w, 2); 
    return (alpha <= beta);
}

int main()
{
    Lorentz eq1;
    cin >> eq1;
    cout << eq1;
    Lorentz eq2(3, 5, 7, 10);
    Lorentz eq;
    eq = eq1+eq2;
    cout << eq;
    return 0;
}